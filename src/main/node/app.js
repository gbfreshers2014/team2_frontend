'use strict';

var express = require('express'),
    libmojito = require('mojito'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    methodOverride = require('method-override'),
    flash = require('connect-flash'),
    passport = require('./authentication').passport,
    app;

app = express();
app.set('port', process.env.PORT || 8661);
libmojito.extend(app, {
    context: {
        environment: "development"
    }
});

app.use(cookieParser());
app.use(bodyParser());
app.use(methodOverride());
app.use(session({ secret: 'gwn dog start' }));
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());





app.use(libmojito.middleware());
app.mojito.attachRoutes();

app.get('/create',passport.ensureAuthenticated, libmojito.dispatch("create_frame.index"));
app.get('/',libmojito.dispatch("login_frame.index"));
//app.post('/',libmojito.dispatch("login.authenticate"));
app.post('/', passport.authenticate('local', { failureRedirect: '/?attempt=1', failureFlash: true }),
    function(req, res) {
        console.log(req.user);
        //validation here
        if(req.user.role === 'admin')
        {
            console.log('11111111111');
            res.redirect('/userprofile/'+req.user.emailId);
        }
        else if(req.user.role === 'employee') {

            console.log('122222');
            //alert('only admin can login');
            res.redirect('/userprofile/'+req.user.emailId);
            res.end('Only Admin can login, please try again');
        }
        else
        {
            //error messages here

            res.end('Server error, please try again');
        }

    });

app.get('/userprofile',passport.ensureAuthenticated,libmojito.dispatch("adminviewprofile_frame.index"));
app.get('/userprofile/:emailId',passport.ensureAuthenticated,libmojito.dispatch("adminviewprofile_frame.index"));

app.post('/userprofile',passport.ensureAuthenticated,libmojito.dispatch("adminviewprofile.submitedit"));

app.post('/changepassword',passport.ensureAuthenticated,libmojito.dispatch("adminviewprofile.changepwd"));




app.get('/uploadattendance',passport.ensureAuthenticated,libmojito.dispatch("upload_frame.index"));

//app.get('/employeeviewprofile/:emailId',passport.ensureAuthenticated,libmojito.dispatch("employeeviewprofile_frame.index"));



app.get('/search',passport.ensureAuthenticated,libmojito.dispatch("searchadmin_frame.index"));
app.post('/search',passport.ensureAuthenticated,libmojito.dispatch("searchadmin.searchspecific"));

app.post('/create',passport.ensureAuthenticated,libmojito.dispatch("create_profile.submitbutton"));

app.get('/leaves',passport.ensureAuthenticated,libmojito.dispatch("leaves_frame.index"));

app.get('/area',passport.ensureAuthenticated,libmojito.dispatch("area_frame.index"));
app.post('/area',passport.ensureAuthenticated,libmojito.dispatch("area.searcharea"));

app.post('/uploadattendance',passport.ensureAuthenticated,libmojito.dispatch("upload_frame.index"));
app.post('/deleteProfile',passport.ensureAuthenticated,libmojito.dispatch("searchadmin_frame.deleteUser"));

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

app.listen(app.get('port'), function () {
    console.log('Server listening on port ' + app.get('port') + ' ' +
                   'in ' + app.get('env') + ' mode');
});

 module.exports = app;