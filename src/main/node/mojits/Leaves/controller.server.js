
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI:false*/

YUI.add('leaves', function(Y, NAME) {
'use strict';
/**
 * The leaves module.
 *
 * @module leaves
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            var user= ac._adapter.req.user.emailId;
            ac.models.get('leave').getreport(function (err, response) {
                var report = [];
                //console.log(res);

                // console.log(response.searchlist.length);

                for (var i = 0; i < response.reportlist.length; i++) {

                    response.reportlist[i].sno = i + 1;

                    report.push(response.reportlist[i]);
                }
                ac.done({leaves: report,username:user}, "leaves");

            });

           //ac.done({},"leaves");
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon']});
