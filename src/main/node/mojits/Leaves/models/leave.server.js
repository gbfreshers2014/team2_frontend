/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI:false*/

YUI.add('leaves-model-leave', function (Y, NAME) {
    'use strict';
    /**
     * The leaves-model module.
     *
     * @module leaves
     */

    /**
     * Constructor for the LeavesModel class.
     *
     * @class LeavesModel
     * @constructor
     */

    var request = require("request");
    var CONFIG = require('config');

    Y.namespace('mojito.models')[NAME] = {

        init: function (config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function (callback) {
            callback(null, { some: 'data' });
        },

        getreport: function (callback) {
            request({

                url: CONFIG.endpoints.leaves,
                //leaves

                method: "GET",

                json: true

            }, function (err, res, body) {
                //console.log(err || body);
                //console.log(typeof body);
                callback(null, {reportlist: body.report});

            });
        }

    };

}, '0.0.1', {requires: []});
