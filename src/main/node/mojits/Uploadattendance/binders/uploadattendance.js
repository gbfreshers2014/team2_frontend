
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('uploadattendance-binder-index', function(Y, NAME) {
    'use strict';
/**
 * The uploadattendance-binder-index module.
 *
 * @module uploadattendance-binder-index
 */

    /**
     * Constructor for the UploadattendanceBinderIndex class.
     *
     * @class UploadattendanceBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            //this.bindUpload(node);
        }

        /*

        bindUpload: function(node) {
            function btnClick() {
                var cfg = {
                    method: 'POST',
                    form: {
                        id: filepath,
                        upload: true
                    }
                };
                /*

                Y.io("/uploadattendance", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    form: JSON.stringify({id: filepath, upload:true}),
                    on: {
                        start: function() {
                            alert("started..!!");
                        },
                        complete: function () {
                            alert("completed..!!");
                        }
                    }
                });



                function start(id, args) {
                    var id = id; // Transaction ID.
                    var args = args.foo; // 'bar'
                    alert("started..!!");
                };

                function complete(id, o, args) {
                    var id = id; // Transaction ID.
                    var data = o.responseText; // Response data.
                    var args = args[1]; // 'ipsum'.
                    alert("completed..!!");
                };
                Y.on('io:start', start, Y, { 'foo': 'bar' });
                Y.on('io:complete', complete, Y, ['lorem', 'ipsum']);
                var request = Y.io("/uploadattendance", cfg);

            }
            Y.one('#button1').on("click", btnClick);
        }
*/
    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'io-upload-iframe']});
