/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('uploadattendance', function(Y, NAME) {
    'use strict';
/**
 * The uploadattendance module.
 *
 * @module uploadattendance
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            //var user= ac._adapter.req.user.emailId;
            //ac.done({username:user},"uploadattendance");
            ac.done({},"uploadattendance");
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-data-addon',
    'mojito-models-addon', "mojito-params-addon","mojito-http-addon"]});
