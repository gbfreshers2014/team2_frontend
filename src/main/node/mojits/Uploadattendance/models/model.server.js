
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('uploadattendance-model', function(Y, NAME) {
    'use strict';
/**
 * The uploadattendance-model module.
 *
 * @module uploadattendance
 */

    /**
     * Constructor for the UploadattendanceModel class.
     *
     * @class UploadattendanceModel
     * @constructor
     */
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        }

    };

}, '0.0.1', {requires: []});
