
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('header2-model', function(Y, NAME) {
'use strict';
/**
 * The header2-model module.
 *
 * @module header2
 */

    /**
     * Constructor for the header2Model class.
     *
     * @class header2Model
     * @constructor
     */
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        }

    };

}, '0.0.1', {requires: []});
