/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI:false*/

YUI.add('login-model-authenticate', function(Y, NAME) {

var request = require("request");
    var CONFIG=require('config');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        
        getData: function(callback) {
            callback(null, { some: 'data' });
        },


        auth: function(un,ps,callback)
        {
            request({
                //url: "http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/hello-world.json"
                //url:"http://14.98.52.234:8080/a-1.0.0-SNAPSHOT/hello-world.json"
                //url:"http://localhost:8080/a-1.0.0-SNAPSHOT/hello-world.json",
                url:CONFIG.endpoints.logine+un+".json",
                //headers: {"Content-Type": "application/json"},
                method: "POST",
                body: {
                        "username" :un,
                        "password" :ps
                      },
                json: true

            }, function(err,res, body){
                console.log(err || body);
                console.log(typeof body);
                callback(null,{ms: body.status.message,em: body.emailId});

            });
        }




    };

}, '0.0.1', {requires: []});
