/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI:false*/

YUI.add('login', function (Y, NAME) {


    Y.namespace('mojito.controllers')[NAME] = {


        index: function (ac) {

            //console.log('inside index');
            var attempt=ac.params.getFromUrl("attempt");
            console.log("attempt========="+attempt);
            if(parseInt(attempt) === 1){
                ac.done({msg:'invalid username/password'}, "login");}
            else
            { ac.done({}, "login");}

        },

        authenticate: function (ac) {
            var un = ac.params.getFromBody("username");
            var ps = ac.params.getFromBody("password");


            ac.models.get('authenticate').auth(un, ps, function (err, response) {
                if (err) {
                    ac.done({msg: "Error"}, "json");
                }

                else {
                    //ac.done({msg:response.authen},"json");
                    ac.done({msg: response.ms, email: response.em}, "json");

                }

            });

//var ty=ac.params.getFromBody("username");
//console.log(ty);

            /*
             if(ac.params.getFromBody("username")=="rupal" && ac.params.getFromBody("password")=="bansal")
             {
             ac.done({msg:"success" },"json");
             }
             else
             {
             ac.done({msg:"error" },"json");

             }
             */

        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-data-addon',
    'mojito-models-addon', "mojito-params-addon", "mojito-http-addon"]});
