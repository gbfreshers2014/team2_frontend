

/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('area-model-areasearch', function(Y, NAME) {
'use strict';
/**
 * The area-model module.
 *
 * @module area
 */

    /**
     * Constructor for the AreaModel class.
     *
     * @class AreaModel
     * @constructor
     */
    var request = require("request");
    var CONFIG=require('config');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        searchspecific: function(country,state,city,callback) {


                request({

                    url:  CONFIG.endpoints.areasearch+ country + "&state=" + state + "&city=" + city,
                    //userprofile
                    //url: "http://192.168.1.108:8080/employee-management-t2-service-1.0.0-SNAPSHOT/userprofiles.json?country="+country,
                    //+
                    //"?country=US",//+country,
                    // +"&state="+state+"&city="+city,

                    //http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/userprofiles.json?emailId=ab&firstName=ab&lastName=ab
                    //headers: {"Content-Type": "application/json"},
                    method: "GET",
                    //body: {

                    //    },
                    json: true

                }, function (err, res, body) {
                    console.log(err || body);
                    console.log(typeof body);
                    callback(null, {arealist: body.users});

                    //callback(null,{frstname: body.up.firstName});

                });


        }

    };
}, '0.0.1', {requires: []});
