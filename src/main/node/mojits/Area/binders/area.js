/*jslint anon:true, sloppy:true, nomen:true*/

/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('area', function(Y, NAME) {

/**
 * The area-binder-index module.
 *
 * @module area-binder-index
 */

    /**
     * Constructor for the AreaBinderIndex class.
     *
     * @class AreaBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.Bindbuttons(node);

        },
        Bindbuttons:function(node)
        {

            function btnclick() {
//alert("bu");
                var co = Y.one('#co').get('value');
                var st = Y.one("#st").get('value');
                var ci = Y.one("#ci").get('value');
                Y.io("/area", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({co:co,st:st,ci:ci}),
                    on: {
                        complete: function (id, o, args) {

                            //searchlist
                            if(o.responseText) {
                                //alert("response")

                                //  if (response.body.status.message == "success") {
                                Y.one('#empty').setHTML(o.responseText);
                                Y.one('#searcharea').setHTML(Y.one('#empty #searcharea').getHTML());
                                Y.one('#empty').setHTML('');
                            }


                        }
                    }

                });


            }
            Y.one('#b1').on("click",btnclick);

        }

    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click']});
