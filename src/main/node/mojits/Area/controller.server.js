
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('area', function(Y, NAME) {
'use strict';
/**
 * The area module.
 *
 * @module area
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {

            var user= ac._adapter.req.user.emailId;
            ac.done({username:user},"area");
        },


        searcharea:function(ac)
        {
            var user= ac._adapter.req.user.emailId;
            var country = ac.params.getFromBody("co");
            var state = ac.params.getFromBody("st");
            var city = ac.params.getFromBody("ci");

            //console.log("controller");
            //console.log(state);

            ac.models.get('areasearch').searchspecific(country,state,city,function (err, response) {
                var arealist = [];
                //console.log(res);
                //console.log('before');
                console.log(response.arealist.length);

                for (var i = 0; i < response.arealist.length; i++) {

                    response.arealist[i].sno = i + 1;

                    response.arealist[i].country = response.arealist[i].addresses[0].country;
                    response.arealist[i].state = response.arealist[i].addresses[0].state;
                    response.arealist[i].city = response.arealist[i].addresses[0].city;

                    //arealist1.push();
                    arealist.push(response.arealist[i]);

                    //arealist.push(arealist1);
                }
                ac.done({arealist: arealist,username:user}, "areanew");

            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-data-addon',
    'mojito-models-addon', "mojito-params-addon","mojito-http-addon"]});
