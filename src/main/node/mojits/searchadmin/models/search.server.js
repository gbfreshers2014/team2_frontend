/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('searchadmin-model-search', function(Y, NAME) {
    'use strict';
/**
 * The searchadmin-model module.
 *
 * @module searchadmin
 */

    /**
     * Constructor for the searchadminModel class.
     *
     * @class searchadminModel
     * @constructor
     */

    var request = require("request");
    var CONFIG = require('config');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        getsearch: function(callback) {
            request({

            url:CONFIG.endpoints.searchadmin+".json",
                  //userprofile
                //headers: {"Content-Type": "application/json"},

                method: "GET",
                //body: {

                //    },
                json: true

            }, function(err,res, body){
                //console.log(err || body);
                //console.log(typeof body);
                callback(null,{searchlist:body.users});

                //callback(null,{frstname: body.up.firstName});

            });

        },

        searchspecific: function(searchobj,callback)
        {
            request({

                //url:"http://induction-dev.gwynniebee.com:8080/employee_information_management_t2-v1/userprofiles.json?" +
                    //"emailId="+searchobj+"&firstName="+searchobj+"&lastName="+searchobj,

                url:CONFIG.endpoints.searchadmin+".json?"+"emailId="+searchobj+"&firstName="+searchobj+"&lastName="+searchobj,

                //headers: {"Content-Type": "application/json"},
                method: "GET",
                //body: {

                //    },
                json: true

            }, function(err,res, body){
                //console.log(err || body);
                //console.log(typeof body);
                callback(null,{searchlist:body.users});



            });
        },

        deleteUserProfile: function(user, toBeDeleted, callback) {
            //url wrong
            var url = "http://induction-dev.gwynniebee.com:8080/employee_information/userprofiles";
            Y.mojito.lib.REST.DELETE(url, JSON.stringify({
                "username": user
            }), {
                headers: {"Content-Type": "application/json"},
                timeout: 12000
            }, function (err, response) {
                console.log(err, '--err');
                console.log(response, '--response');
                if(response) {
                    callback(null, {res: response});
                }
                else {
                    callback(null,null);
                }
            });
        }

    };

}, '0.0.1', {requires: []});
