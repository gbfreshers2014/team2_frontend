
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('searchadmin', function(Y, NAME) {
'use strict';
/**
 * The searchadmin module.
 *
 * @module searchadmin
 */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */


        index: function (ac) {

            var user = ac._adapter.req.user.emailId;
            var role = ac._adapter.req.user.role;

             ac.models.get('search').getsearch(function (err, response) {
                    var searchlist = [];
                    //console.log(err);

                    //console.log(response.searchlist, '===');
                // console.log(response.searchlist, '==ser');

                    for (var i = 0; i < response.searchlist.length; i++) {

                        response.searchlist[i].sno = i + 1;

                        searchlist.push(response.searchlist[i]);
                    }



            if (role === 'admin' || role === 'Admin') {
                ac.done({searchlist: searchlist, username: user}, "searchadmin");
            }
            else {
                 ac.done({searchlist: searchlist, username: user}, "searchemp");

            }
                });


        },

        searchspecific: function (ac) {

            var searchobj = ac.params.getFromBody("searchnn");
            var user = ac._adapter.req.user.emailId;
            var role = ac._adapter.req.user.role;

            ac.models.get('search').searchspecific(searchobj, function (err, response) {
                var searchlist = [];


                //console.log(response.searchlist.length);

                for (var i = 0; i < response.searchlist.length; i++) {

                    response.searchlist[i].sno = i + 1;

                    searchlist.push(response.searchlist[i]);
                }
                if(role=== 'admin'|| role=== 'Admin') {
                    ac.done({searchlist: searchlist, username: user}, "searchadminnew");
                } else {
                    ac.done({searchlist: searchlist, username: user}, "searchempnew");
                }


            });




        },

        deleteUser: function(ac) {
            var toBeDeleted = ac.params.getFromBody("email");
            var user = ac._adapter.req.user.emailId;
            //var pwd = ac._adapter.req.user.emailId;

            ac.models.get('search').deleteUserProfile(user, toBeDeleted, function (err, response) {
                if (err) {
                    ac.error(err);
                    return;
                }
                else {
                    ac.done({body: response.res._resp.responseText}, "json");
                }
            });

        }



    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon','mojito-params-addon']});
