
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('searchemp', function(Y, NAME) {
    'use strict';
    /**
     * The searchadmin-binder-index module.
     *
     * @module searchadmin-binder-index
     */

    /**
     * Constructor for the searchadminBinderIndex class.
     *
     * @class searchadminBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.Bindbuttons(node);
            /**
             * Example code for the bind method:
             *
             * node.all('dt').on('mouseenter', function(evt) {
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).addClass('sel');
             *
             * });
             * node.all('dt').on('mouseleave', function(evt) {
             *
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).removeClass('sel');
             *
             * });
             */
        },
        Bindbuttons: function(node)
        {

            function btnclick() {

                var searchnn = Y.one('#searchnn').get('value');
                //var role= ac._adapter.req.user.role;
                //alert(role);
                Y.io("/search", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({searchnn:searchnn}),
                    on: {
                        complete: function (id, o, args) {

                            //searchlist
                            if(o.responseText) {

                                //  if (response.body.status.message == "success") {
                                Y.one('#empty').setHTML(o.responseText);
                                Y.one('#searchTable').setHTML(Y.one('#empty #searchTable').getHTML());
                                Y.one('#empty').setHTML('');
                            }


                        }
                    }

                });




            }
            Y.one('#button1').on("click",btnclick);

        }

    };

}, '0.0.1', {requires: ['event-mouseenter','event-click' ,'mojito-client']});
