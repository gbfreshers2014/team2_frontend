
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI,alert*/

YUI.add('searchadmin', function(Y, NAME) {
    'use strict';
/**
 * The searchadmin-binder-index module.
 *
 * @module searchadmin-binder-index
 */

    /**
     * Constructor for the searchadminBinderIndex class.
     *
     * @class searchadminBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            this.Bindbuttons(node);
            this.bindDelete(node);
        },

        bindDelete: function(node) {
            function btnclick() {
                var email = Y.one('#emailId').getAttribute('data-email'),
                    rowId = '#row_'+email,
                    row = Y.one(rowId);

                Y.io("/deleteProfile", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({email: email}),
                    on: {
                        complete: function (id, o, args) {
                            var response = JSON.parse(o.responseText),
                                response1 = JSON.parse(response.body);
                            if (o.responseText) {
                                if (response1.message === "Success") {
                                    alert("Item deleted from Inventory..!!");
                                    row.ancestor().removeChild(row);
                                } else {
                                    alert("Item cannot be deleted..!!");
                                }
                            }
                        }
                    }
                });
            }
            Y.one('#deleteButton').on("click",btnclick);
        },

        Bindbuttons: function(node)
        {
            function btnclick() {

                var searchnn = Y.one('#searchnn').get('value');
                //var role= ac._adapter.req.user.role;
                //alert(role);
                Y.io("/search", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: JSON.stringify({searchnn:searchnn}),
                    on: {
                        complete: function (id, o, args) {

                            //searchlist
                            if(o.responseText) {

                                //  if (response.body.status.message == "success") {
                                Y.one('#empty').setHTML(o.responseText);
                                Y.one('#searchTable').setHTML(Y.one('#empty #searchTable').getHTML());
                                Y.one('#empty').setHTML('');
                                 }


                            }
                    }

                });




                }
            Y.one('#button1').on("click",btnclick);
            
        }

    };

}, '0.0.1', {requires: ['event-mouseenter','event-click' ,'mojito-client']});
