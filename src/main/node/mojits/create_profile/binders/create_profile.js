/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI,alert*/


YUI.add('create_profile', function (Y, NAME) {
    'use strict';
    Y.namespace('mojito.binders')[NAME] = {
        init: function (mojitProxy) {
            this.mojitProxy = mojitProxy;
        },
        bind: function (node) {
            var me = this;
            this.node = node;
            this.verfication(node);
            this.bindVendorButtons(node);
        },

        verfication: function (node) {

            function valChange() {
                var firstName = Y.one('#fn').get('value');
                var lastName = Y.one('#ln').get('value');
                var dateofjoining= Y.one('#doj').get('value');


                if (firstName.length >= 2 && lastName.length >=2 && dateofjoining.length===10) {
                    // Y.one('#button1').setStyle('background-color',"Red");
                    Y.one('#button1').set('disabled', false);
                }

            }

            Y.one('#fn').on("blur", valChange);
            Y.one('#ln').on("blur", valChange);
            Y.one('#doj').on("blur", valChange);

        },
        bindVendorButtons: function (node) {
            //alert('before btn click');


            function btncancel() {
                //alert("cancel button");
                Y.config.win.location = '/userprofile';
            }

            function btnclick() {
                //e.preventDefault();
                var firstName = Y.one('#fn').get('value');
                var lastName = Y.one('#ln').get('value');
                var emailid = Y.one('#Em').get('value');
                var DOJ = Y.one('#doj').get('value');
                var accountType = Y.one('#At').get('value');
                var employeeStatus = Y.one('#Es').get('value');

                /*if(emailid == "")
                 {
                 alert('Enter emailid');
                 }*/

                //alert('after btn clikc');
                //if(emailid.contains("@")||emailid.contains(".")|| emailid=="")


                Y.io("/create", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: //JSON.stringify({firstName: firstName})
                        JSON.stringify({firstName: firstName, lastName: lastName, emailid: emailid, DOJ: DOJ,
                            accountType: accountType, employeeStatus: employeeStatus }),
                    on: {
                        complete: function (id, o, args) {

                            var response = JSON.parse(o.responseText);


                            if (response.msg === 'Success') {
                                alert("Profile has been successfully created\n username and password will\n be mailed at " +
                                 "the given email address");
                                Y.config.win.location = '/userprofile';

                            }
                            //alert(response.msg);
                            else {
                                alert("Error occurred!!!!!!!!");
                                Y.config.win.location = '/create';
                            }


                            //alert(response.msg+response.msg1+response.msg2+response.msg3+response.msg4+response.msg5);
                        }
                    }

                });


            }

            Y.one('#button1').on("click", btnclick);
            Y.one('#button2').on("click", btncancel);

        }
    };
}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click']});
