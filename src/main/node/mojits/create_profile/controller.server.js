/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/


YUI.add('create_profile', function (Y, NAME) {
        'use strict';


        Y.namespace('mojito.controllers')[NAME] = {


            index: function (ac) {
                var userid = ac._adapter.req.user.emailId;
                //console.log('inside index');
                ac.done({username: userid}, "create_profile");
            },

            submitbutton: function (ac) {
                //console.log('inside submitbutton');
                var userid = ac._adapter.req.user.emailId;
                var fs = ac.params.getFromBody("firstName");
                var ls = ac.params.getFromBody("lastName");
                var em = ac.params.getFromBody("emailid");
                var dj = ac.params.getFromBody("DOJ");
                var at = ac.params.getFromBody("accountType");
                var es = ac.params.getFromBody("employeeStatus");

                //if( fs==NULL && ls==NULL)
                //ac.done({msg:"success"},"json");

                //params ={ frst:fs, lst:ls , ema:em, doj:dj, acc:at, ess:es };
                //console.log(at);

                ac.models.get('create').create_prof(userid, fs, ls, em, dj, at, es, function (err, response) {
                    if (err) {
                        ac.done({msg: "Error"}, "json");
                    }

                    else {
                        ac.done({msg: response.mes}, "json");
                        //ac.done({msg:"Profile has been created"},"json");

                    }

                });
            }


            /*else
             {
             ac.done({msg:"error"},"json");
             }*/
            //ac.done({msg:fs,msg1:ls,msg2:em,msg3:dj,msg4:at,msg5:es},"json");
            /* if(ac.params.getFromBody("firstName")=="r")
             {
             ac.done({msg:"success" },"json");
             }
             else
             {
             ac.done({msg:"error" },"json");

             }*/


        };
    },
    '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-data-addon',
        'mojito-models-addon', "mojito-params-addon", "mojito-http-addon"]}
)
;
