/*jslint anon:true, sloppy:true, nomen:true*/

/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('create_profile-model-create', function(Y, NAME) {


   var request = require("request");

    var CONFIG=require('config');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        
        getData: function(callback) {
            callback(null, { some: 'data' });
        },

        create_prof: function(userid,fs,ls,em,dj,at,es,callback)
        {   //console.log(at);
            //var url = "http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/hello-world.json";
            request({
                //url: "http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/hello-world.json"
                //url:"http://14.98.52.234:8080/a-1.0.0-SNAPSHOT/hello-world.json"
                //url:"http://localhost:8080/a-1.0.0-SNAPSHOT/hello-world.json",
                url:CONFIG.endpoints.createprofile,
                //userprofile
                //url:"http://192.168.1.108:8080/a-1.0.0-SNAPSHOT/userprofiles.json",
                //headers: {"Content-Type": "application/json"},
                method: "POST",
                body: {
                        "email" :em,
                        "firstName" : fs,     //optional
                        "lastName" : ls,      //optional
                        "DOJ"         : dj,
                        "accountType" : at,
                        "employementStatus" : es,
                        "createdBy":userid
                      },
                json: true

            }, function(err,res, body){
                //console.log(err || body);
                //console.log(typeof body);
                callback(null,{mes: body.status.message});

            });
                // requestHeaders = {
                //     contentType: "application/json"
                // },
                // requestParams = {
                //     frst:fs, lst:ls , ema:em, doj:dj, acc:at, ess:es
                // },
                // responseData;

                // Y.mojito.lib.REST.POST(url, requestParams, requestHeaders, function (err, response) {
                // if (err) {
                //     return callback(err);
                // }

                //responseData = JSON.parse(response.getBody());

                


            //});


        }






    };

}, '0.0.1', {requires: ['mojito-rest-lib', 'config']});
