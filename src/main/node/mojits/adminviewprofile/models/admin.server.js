/*jslint anon:true, sloppy:true, nomen:true*/
/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/

YUI.add('adminviewprofile-model-admin', function (Y, NAME) {

    /**
     * The adminviewprofile-model module.
     *
     * @module adminviewprofile
     */

    /**
     * Constructor for the adminviewprofileModel class.
     *
     * @class adminviewprofileModel
     * @constructor*/

    var request = require("request");
    var CONFIG=require('config');

    Y.namespace('mojito.models')[NAME] = {

        init: function (config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function (callback) {
            callback(null, { some: 'data' });
        },

        getprofile: function (user, callback) {

            request({
                url: CONFIG.endpoints.adminview+ user + ".json",
                //url:userprofile+user+".json"
                //userprofile

                method: "GET",
                //body: {

                //    },
                json: true

            }, function (err, res, body) {
                //console.log(err || body.details.addresses);
                //console.log(typeof body);
                //callback(null,body);

                callback(null, {view: body.details});

            });


        },


        edit_prof: function (userid,ac, callback) {



            request({
                //url: "http://induction-dev.gwynniebee.com:8080/employee_information_management_t2-v1/userprofiles/"+userid+".json",
                url: CONFIG.endpoints.adminview + userid + ".json",

                //userprofile

                method: "PUT",
                body: {
                    "emailId": ac.params.getFromBody("emailid"),
                    "firstName":ac.params.getFromBody("firstName"),     //optional
                    "lastName": ac.params.getFromBody("lastName"),      //optional
                    "dateOfBirth": ac.params.getFromBody("DOB"),
                    "dateOfJoining":ac.params.getFromBody("DOJ"),
                    "bloodGroup":ac.params.getFromBody("Bg"),
                    "leavesTaken":ac.params.getFromBody("leavestaken"),
                    "role":ac.params.getFromBody("role"),
                    "skypeId":ac.params.getFromBody("sky"),
                    "phoneNo":ac.params.getFromBody("phn"),
                    "createdOn":ac.params.getFromBody("cron"),
                    "createdBy":ac.params.getFromBody("crby"),
                    "updatedBy":userid,

                    "addresses":[
                        { "status": "permanent",
                            "line1": ac.params.getFromBody("line1"),
                            "line2": ac.params.getFromBody("line2"),
                            "country": ac.params.getFromBody("country"),
                            "state": ac.params.getFromBody("state"),
                            "city": ac.params.getFromBody("city"),
                            "zipcode": ac.params.getFromBody("zipcode"),
                            "createdOn":ac.params.getFromBody("cron") ,
                            "createdBy":ac.params.getFromBody("crby") ,
                            "updatedBy":userid
                        },
                        {   "status":"temporary",
                            "line1": ac.params.getFromBody("tline1"),
                            "line2": ac.params.getFromBody("tline2"),
                            "country": ac.params.getFromBody("tcountry"),
                            "state": ac.params.getFromBody("tstate"),
                            "city": ac.params.getFromBody("tcity"),
                            "zipcode": ac.params.getFromBody("tzipcode"),
                            "createdOn":ac.params.getFromBody("cron") ,
                            "createdBy":ac.params.getFromBody("crby") ,
                            "updatedBy":userid
                        }
                        ]

                },
                json: true

            }, function (err, res,body) {
                //console.log(err || body);
                console.log(body, "body");
                callback(null, {mes: body.message});

            });


        },

        change_pswd: function (oldpsw, newpsw, user, callback) {
            request({

                url:CONFIG.endpoints.changepwd + user + ".json",
//authenticate
                method: "PUT",
                body: {
                    "oldPassword": oldpsw,
                    "newPassword": newpsw,
                    "userName": user,
                    "reset": false

                },
                json: true

            }, function (err, res, body) {
                console.log(err || body);
                console.log(typeof body);
                callback(null, {mes: body.message});

            });

        }



    };
}, '0.0.1', {requires: ['mojito-rest-lib',"mojito-params-addon"]});


