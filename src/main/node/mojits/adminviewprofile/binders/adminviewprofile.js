/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI,alert,panel*/

YUI.add('adminviewprofile', function (Y, NAME) {
    'use strict';
    /**
     * The adminviewprofile-binder-index module.
     *
     * @module adminviewprofile-binder-index
     */

    /**
     * Constructor for the adminviewprofileBinderIndex class.
     *
     * @class adminviewprofileBinderIndex
     * @constructor
     */
    var panel = new Y.Panel({
        srcNode: '#panelContent',
        headerContent: 'Change User Password',
        width: 250,
        zIndex: 5,
        centered: true,
        modal: true,
        visible: false,
        render: true,
        plugins: [Y.Plugin.Drag]
    });

    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function (mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function (node) {
            var me = this;
            this.node = node;
            this.bindbutton(node);
            this.bindedit(node);
            this.bindpanel(node);
            this.bindpanelbuttons(node);
            this.verfication(node);
            /**
             * Example code for the bind method:
             *
             * node.all('dt').on('mouseenter', function(evt) {
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).addClass('sel');
             *
             * });
             * node.all('dt').on('mouseleave', function(evt) {
             *   
             *   var dd = '#dd_' + evt.target.get('text');
             *   me.node.one(dd).removeClass('sel');
             *
             * });
             */
        },
        verfication: function (node) {

            function valChange() {
                var firstName = Y.one('#fname').get('value');
                var lastName = Y.one('#lname').get('value');
                var dateofjoining= Y.one('#doj').get('value');


                if (firstName.length >= 2 && lastName.length >=2 &&dateofjoining.length===10) {
                    // Y.one('#button1').setStyle('background-color',"Red");
                    Y.one('#save').set('disabled', false);
                }

            }

            Y.one('#fname').on("blur", valChange);
            Y.one('#lname').on("blur", valChange);
            Y.one('#doj').on("blur", valChange);

        },
        bindpanel: function (node) {

            function btnpswd() {

                Y.one('#dis').setStyle("display", 'block');
                panel.show();
            }

            Y.one('#change').on("click", btnpswd);
        },
        bindpanelbuttons: function (node) {


            function btncancel() {
                Y.one('#dis').setStyle("display", 'none');
                panel.hide();
            }

            function changepswd(e) {
                e.preventDefault();
                var oldpwd = Y.one('#old').get('value');
                var newpwd = Y.one('#new').get('value');
                //var a=Y.one('#la').get('textContent');
                //alert(a);


                //alert(a);

                Y.io("/changepassword", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: //JSON.stringify({firstName: firstName})
                        JSON.stringify({newpwd: newpwd, oldpwd: oldpwd}),
                    on: {
                        complete: function (id, o, args) {

                            var response = JSON.parse(o.responseText);


                            if (response.msg === 'Success') {
                                // alert("Profile has been successfully edited");
                                Y.config.win.location = '/userprofile';

                            }
                            //alert(response.msg);
                            else {
                                alert("Error occurred!!!!!!!!");
                                Y.config.win.location = '/userprofile';
                            }


                            //alert(response.msg+response.msg1+response.msg2+response.msg3+response.msg4+response.msg5);
                        }
                    }

                });

            }

            Y.one('#button1').on("click", changepswd);
            Y.one('#button2').on("click", btncancel);
        },

        bindbutton: function (node) {

            function btnclick() {
                Y.all(".e").setStyle("display", 'block');
                Y.all(".v").setStyle("display", 'none');
            }

            Y.one('#edit').on("click", btnclick);
        },

        bindedit: function (node) {


            function btncancel() {
                //alert("cancel button");
                var emailid = Y.one('#email').get('textContent');
               // Y.one('#divedit').setStyle("display", 'block');
                Y.config.win.location = '/userprofile/'+emailid;
            }

            function btnclick() {
                //e.preventDefault();


                var firstName = Y.one('#fname').get('value');
                var lastName = Y.one('#lname').get('value');

                var DOB = Y.one('#dob').get('value');
                var DOJ = Y.one('#doj').get('value');
                var Bg = Y.one('#bg').get('value');
                var phn = Y.one('#phonenumber').get('value');
                var sky = Y.one('#skypeid').get('value');

                var role = Y.one('#role').get('textContent');
                var emailid = Y.one('#email').get('textContent');
                var leavestaken = Y.one('#leavestaken').get('textContent');
                var cron = Y.one('#createdon').get('textContent');
                var crby = Y.one('#createdby').get('textContent');

                var line1 = Y.one('#line1').get('value');
                var line2 = Y.one('#line2').get('value');
                var country = Y.one('#country').get('value');
                var city = Y.one('#city').get('value');
                var state = Y.one('#state').get('value');
                var zipcode = Y.one('#zipcode').get('value');

                var tline1 = Y.one('#tline1').get('value');
                var tline2 = Y.one('#tline2').get('value');
                var tcountry = Y.one('#tcountry').get('value');
                var tcity = Y.one('#tcity').get('value');
                var tstate = Y.one('#tstate').get('value');
                var tzipcode = Y.one('#tzipcode').get('value');




                /*if(emailid == "")
                 {
                 alert('Enter emailid');
                 }*/

                //alert('after btn clikc');
                //if(emailid.contains("@")||emailid.contains(".")|| emailid=="")


                Y.io("/userprofile", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: //JSON.stringify({firstName: firstName})
                        JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            DOB: DOB,
                            DOJ: DOJ,
                            Bg: Bg,
                            phn: phn,
                            sky: sky,
                            role: role,
                            emailid: emailid,
                            leavestaken: leavestaken,
                            line1: line1,
                            line2: line2,
                            country: country,
                            state: state,
                            city: city,
                            zipcode: zipcode,
                            tline1: tline1,
                            tline2: tline2,
                            tcountry: tcountry,
                            tcity: tcity,
                            tstate: tstate,
                            tzipcode: tzipcode,
                            cron: cron, crby: crby}),
                    on: {
                        complete: function (id, o, args) {

                            var response = JSON.parse(o.responseText);


                            if (response.msg === 'Success') {
                                alert("Profile has been successfully edited");
                                Y.config.win.location = '/userprofile/'+emailid;

                            }
                            //alert(response.msg);
                            else {
                                alert("Error occurred!!!!!!!!");
                                Y.config.win.location = '/userprofile/'+emailid;
                            }


                            //alert(response.msg+response.msg1+response.msg2+response.msg3+response.msg4+response.msg5);
                        }
                    }

                });


            }

            Y.one('#save').on("click", btnclick);
            Y.one('#cancel').on("click", btncancel);
        }



    };

}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click', 'panel']});
