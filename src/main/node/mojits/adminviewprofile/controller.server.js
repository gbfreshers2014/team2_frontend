/**
 * Copyright 2013 GwynnieBee Inc.
 */


/*global YUI*/


YUI.add('adminviewprofile', function (Y, NAME) {

    'use strict';
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */

        index: function (ac) {
            //ac.assests.addCSS(./index.css)

            //ac.done({},"adminviewprofile");

            // var us= localstorage.getItem("usrname");

            var userid = ac._adapter.req.user.emailId;
            var useridroutes = ac.params.route('emailId');
            var role = ac._adapter.req.user.role;
            var sameUser = false;
            //var role = ac._adapter.req.user.role;
            //var role=ac._adapter.req.use.
            if (userid === useridroutes) {
                sameUser = true;
            }
            if (!useridroutes) {
                useridroutes = userid;
            }
            var data = {};

            ac.models.get('admin').getprofile(useridroutes, function (err, response) {
                if (err) {
                    ac.done({msg: "Error"}, "json");
                }

                else {
                    data = {
                        fname: response.view.firstName,
                        email: response.view.emailId,
                        lname: response.view.lastName,
                        dob: response.view.dateOfBirth,
                        doj: response.view.dateofJoining,
                        bg: response.view.bloodGroup,
                        sky: response.view.skypeId,
                        phonenumber: response.view.phoneNo,
                        role: response.view.role,
                        status: response.view.addresses.status,
                        username: userid,
                        leavesTaken: response.view.leavesTaken,
                        createdby: response.view.createdBy,
                        createdon: response.view.createdOn


                    };

                    //ac.data.setAttrs({fname:response.frstname});
                    ac.data.setAttrs({fname: response.view.firstName,
                        email: response.view.emailId,
                        lname: response.view.lastName,
                        dob: response.view.dateOfBirth,
                        doj: response.view.dateOfJoining,
                        bg: response.view.bloodGroup,
                        sky: response.view.skypeId,
                        phonenumber: response.view.phoneNo,
                        role: response.view.role,
                        status: response.view.addresses.status,
                        username: userid,
                        leavesTaken: response.view.leavesTaken,
                        createdby: response.view.createdBy,
                        createdon: response.view.createdOn
                    });

                    //console.log(response.frstname);
                    //console.log(response.view.addresses.length);


                    // "status":"permanent","line1":"722 Halsell Ave","line2":"P.O. Box 23","city":"Guaynabo","state":"PR","country":"US","zipcode":"971"

                    for (var i = 0; i < response.view.addresses.length; i++) {
                        if (response.view.addresses[i].status === 'permanent') {
                            data = {

                                line1: response.view.addresses[i].line1, line2: response.view.addresses[i].line2,
                                city: response.view.addresses[i].city, state: response.view.addresses[i].state,
                                country: response.view.addresses[i].country, zipcode: response.view.addresses[i].zipcode


                            };
                            ac.data.setAttrs({line1: response.view.addresses[i].line1, line2: response.view.addresses[i].line2,
                                city: response.view.addresses[i].city, state: response.view.addresses[i].state,
                                country: response.view.addresses[i].country, zipcode: response.view.addresses[i].zipcode});
                        }
                        else {
                            data = {

                                tline1: response.view.addresses[i].line1, tline2: response.view.addresses[i].line2,
                                tcity: response.view.addresses[i].city, tstate: response.view.addresses[i].state,
                                tcountry: response.view.addresses[i].country, tzipcode: response.view.addresses[i].zipcode


                            };
                            ac.data.setAttrs({tline1: response.view.addresses[i].line1, tline2: response.view.addresses[i].line2,
                                tcity: response.view.addresses[i].city, tstate: response.view.addresses[i].state,
                                tcountry: response.view.addresses[i].country, tzipcode: response.view.addresses[i].zipcode});

                        }


                    }
                    data.sameUser = sameUser;
                    if (role === 'admin') {
                        data.admin = true;

                    }
                    else {
                        data.admin = false;

                    }
                    ac.done(data, "adminviewprofile");

                    //ac.done({msg:response.frstname},"json");
                    //ac.done({msg:"Profile has been created"},"json");

                }

            });

        },


        submitedit: function (ac) {


            var userid = ac._adapter.req.user.emailId;
            console.log('---controller');

            ac.models.get('admin').edit_prof(userid, ac, function (err, response) {
                if (err) {
                    ac.done({msg: "Error"}, "json");
                }

                else {
                    console.log('-----at controller return');
                    ac.done({msg: response.mes, userid: userid}, "json");
                    //ac.done({msg:"Profile has been created"},"json");

                }

            });
        },

        changepwd: function (ac) {

            var oldpsw = ac.params.getFromBody("oldpwd");
            var newpsw = ac.params.getFromBody("newpwd");
            //var useridroutes = ac.params.route('emailId');
            var userid = ac._adapter.req.user.emailId;

            ac.models.get('admin').change_pswd(oldpsw, newpsw, userid, function (err, response) {
                if (err) {
                    ac.done({msg: "Error"}, "json");
                }

                else {
                    ac.done({msg: response.mes}, "json");
                    //ac.done({msg:"Profile has been created"},"json");

                }
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-data-addon',
    'mojito-models-addon', "mojito-params-addon", "mojito-http-addon"]});
