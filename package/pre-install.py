#! /usr/bin/env python
#
# Does the following:
# - Runs mojito tests
# - Stops existing service if any
#

import os
import sys

mojito_app_dir = "/home/gb/share/mojito/team2-project"


def main():
    """Main function"""

    print "Running pre-install script..."

    # Stop service if running.
    if os.path.islink("/service/team2-project"):
        os.chdir("/service/team2-project")

        # Remove the symlink
        try:
            print "Unlinking /service/team2-project"
            os.unlink("/service/team2-project")
        except Exception as ex:
            print "Exception in unlinking /service/team2-project", ex

        print "Stopping existing service..."
        command = "svc -dx ."
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team2-project"
            sys.exit(1)
        command = "svc -dx log"
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - team2-project"
            sys.exit(1)
    else:
        print "No existing service found for team2-project"

if __name__ == "__main__":
    main()
